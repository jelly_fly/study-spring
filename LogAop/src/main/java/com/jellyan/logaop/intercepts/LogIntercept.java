package com.jellyan.logaop.intercepts;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.util.MethodInvoker;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.xml.crypto.dsig.SignatureMethod;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.TypeVariable;
import java.util.*;

/**
 * @author jellyan
 * @version 1.0
 * @date 11:49 AM
 * @descr com.jellyan.logaop.intercepts
 */
@Order(5)
@Aspect
@Component
public class LogIntercept {

    private Logger logger = LoggerFactory.getLogger(LogIntercept.class);

    @Pointcut("execution(public * com.jellyan.logaop..*(..))")
    public void logIntercept() {
    }

    /************************** 实现方式一  *****************************************/
    @Before("logIntercept()")
    public void doBefore(JoinPoint point) throws Throwable {
        System.out.println("doBefore ...");
        Object[] args = point.getArgs();

        Signature signature = point.getSignature();
        MethodSignature method = (MethodSignature) signature;
//        String name = method.getMethod().getName();
        String[] params = method.getParameterNames();
        if (params.length == args.length) {
            for (int i = 0; i < params.length; i++) {
                System.out.println(params[i] + " : " + args[i]);
            }
        }
    }

    @AfterReturning(returning = "ret", pointcut = "logIntercept()")
    public void afterReturn(Object ret) {
        System.out.println("afterReturn ...");
        System.out.println(ret);
    }

    @AfterThrowing("logIntercept()")
    public void afterThrowing(JoinPoint point) {
        System.out.println("afterThrowing ...");
        System.out.println(point.getArgs());
    }

    /************************** 实现方式二  *****************************************/

    @Around("logIntercept()")
    public Object around(ProceedingJoinPoint pjp) throws Throwable {
        System.out.println("around .......");

        Object[] args = pjp.getArgs();
        String methodName = pjp.getSignature().getName();
        Class clazz = pjp.getSignature().getDeclaringType();
        /*Method[] methods = clazz.getDeclaredMethods();
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                if (method.getParameters().length == args.length) {
                    Parameter[] params = method.getParameters();
                    for (int i = 0; i < args.length; i++) {
                        System.out.println(params[i] + " >>> " + args[i]);
                    }
                }
            }
        }*/
        Class[]classes = new Class[args.length];
        for (int i = 0;i<args.length;i++){
            if (args[i].getClass().getTypeName().equals("java.lang.Integer")){
                classes[i] = Integer.TYPE;
            }else{
                classes[i] = args[i].getClass();
            }
            System.out.println(args[i].getClass().getTypeName());
        }
        //注意数据类型，反射的时候应用类型和值类型是要区分开的，反射去取数据的时候要确定数据的类型
        Method method = getMethods(clazz,methodName,classes);
        Parameter[] params = method.getParameters();
        for (int i = 0; i < args.length; i++) {
            System.out.println(params[i] + " >>> " + args[i]);
        }

        return pjp.proceed();

    }

    Method getMethods(Class clazz,String name,Class<?> ...classes) throws NoSuchMethodException {
        Method method = clazz.getMethod(name, classes);
//        method.setAccessible(true);
        return method;
    }


}
