package com.jellyan.logaop.web;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jellyan
 * @version 1.0
 * @date 12:23 PM
 * @descr com.jellyan.logaop
 */
@RestController
public class TestController {

    @RequestMapping("/test")
    public String test(@RequestParam("oooo")String oooo){
        return "rest";
    }

    @RequestMapping("/test2")
    public String test2(@RequestParam("oooo")String oooo,@RequestParam("abc")int abc){
        return "rest";
    }

    @RequestMapping("/test1")
    public String test(){
        return "rest";
    }

}
